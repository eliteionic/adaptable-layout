import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  public isDesktop = false;

  constructor(private platform: Platform) {}

  ngOnInit() {
    if (this.platform.is('desktop')) {
      this.isDesktop = true;
    }
  }
}
